var express = require('express');
var session = require("express-session");

var router = express.Router();
var app = express();

app.set('view engine', 'ejs');
app.use('/assets', express.static('assets'));

var index = require('./controllers/index.js');
var blog = require('./controllers/blog.js');
var candidates = require('./controllers/candidates.js');
var checkRegistration = require('./controllers/checkRegistration.js');
var contact = require('./controllers/contact.js');
var gallery1 = require('./controllers/gallery1.js');
var gallery2 = require('./controllers/gallery2.js');
var login = require('./controllers/login.js');
var news = require('./controllers/news.js');
var polls = require('./controllers/polls.js');
var register = require('./controllers/register.js');
var signup = require('./controllers/signup.js');
var request = require('./controllers/request.js');

// set up the session
app.use(
    session({
        secret: "router",
        name: "router",
        resave: true,
        saveUninitialized: true
    })
);
app.use('/', index);
app.use('/index', index);
app.use('/blog', blog);
app.use('/candidates', candidates);
app.use('/checkRegistration', checkRegistration);
app.use('/contact', contact);
app.use('/gallery1', gallery1);
app.use('/gallery2', gallery2);
app.use('/login', login);
app.use('/news', news);
app.use('/polls', polls);
app.use('/register', register);
app.use('/signup', signup);
app.use('/request', request);



app.listen(8084, function () {
    console.log('Listening on port 8084 ... ');
});

module.exports = router;