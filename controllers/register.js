var express = require('express');
var router = express.Router();
var $ = require("jquery");


var logout = function(req, res, next) {
    req.session.loggedIn = undefined;
    res.render('index');
};


router.get('/', function (req, res) {
     //res.render('index', {loggedIn: req.session.loggedIn, username: req.session.loggedUser});
    res.render('register');
});

router.get("/logout", function(req, res) {
    console.log('Destroying session');
    req.session.destroy(); // destroy the current session
    res.redirect('/');
});

router.use("/logout", logout);
module.exports = router;
