var express = require('express');
var router = express.Router();
var $ = require("jquery");


function getLocationsData(res) {
    var str = ""

    locations = res.pollingLocations
    if (locations.length == 0) {
        return "Sorry, there are no polling locations near you.";
    }

    str += "Polling Locations Near You:\n\n";

    for (var i in locations) {
        var address = locations[i].address
        str += "Name: " + address.locationName + "\n\n";
        str += "Address: " + [address.line1, address.city, address.state].join(',') + " " + address.zip + "\n\n";
        str += "Polling Hours: " + locations[i].pollingHours + "\n\n";
        str += "\n\n";
    }
    return str;
}

var logout = function(req, res, next) {
    req.session.loggedIn = undefined;
    res.render('index');
};


router.get('/', function (req, res) {
     //res.render('index', {loggedIn: req.session.loggedIn, username: req.session.loggedUser});
    res.render('polls');
});

router.get("/logout", function(req, res) {
    console.log('Destroying session');
    req.session.destroy(); // destroy the current session
    res.redirect('/');
});

router.use("/logout", logout);
module.exports = router;
